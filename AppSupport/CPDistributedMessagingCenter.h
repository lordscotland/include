@interface CPDistributedMessagingCenter : NSObject
+(CPDistributedMessagingCenter*)centerNamed:(NSString*)name;
-(void)registerForMessageName:(NSString*)message target:(id)target selector:(SEL)selector;
-(void)runServerOnCurrentThread;
-(void)sendMessageAndReceiveReplyName:(NSString*)message userInfo:(NSDictionary*)info toTarget:(id)target selector:(SEL)selector context:(void*)context;
-(BOOL)sendMessageName:(NSString*)message userInfo:(NSDictionary*)info;
-(NSDictionary*)sendMessageAndReceiveReplyName:(NSString*)message userInfo:(NSDictionary*)info;
-(NSDictionary*)sendMessageAndReceiveReplyName:(NSString*)message userInfo:(NSDictionary*)info error:(NSError**)error;
-(void)stopServer;
-(void)unregisterForMessageName:(NSString*)message;
@end
