extern NSString *WebArchivePboardType;

@interface DOMNodeList
-(id)item:(NSUInteger)index;
-(NSUInteger)length;
@end

@interface DOMNode : NSObject
-(DOMNode*)appendChild:(DOMNode*)node;
-(DOMNodeList*)childNodes;
-(DOMNode*)firstChild;
-(DOMNode*)insertBefore:(DOMNode*)node :(DOMNode*)refNode;
-(DOMNode*)nextSibling;
-(DOMNode*)parentNode;
-(DOMNode*)removeChild:(DOMNode*)node;
@end

@interface DOMElement : DOMNode
-(NSString*)getAttribute:(NSString*)name;
-(void)removeAttribute:(NSString*)name;
-(void)setAttribute:(NSString*)name value:(NSString*)value;
@end

@interface DOMRange
-(void)collapse:(BOOL)start;
-(void)deleteContents;
-(void)insertNode:(DOMNode*)node;
-(void)setEndAfter:(DOMNode*)node;
-(void)setStartAfter:(DOMNode*)node;
-(NSString*)text;
@end

@interface DOMHTMLElement : DOMElement
@property(assign) NSString* innerHTML;
@end

@interface DOMHTMLImageElement : DOMHTMLElement
-(NSURL*)absoluteImageURL;
@end

@interface DOMHTMLBodyElement : DOMHTMLElement
@end

@interface DOMText : DOMNode
@end

@interface DOMDocument : DOMNode
-(DOMHTMLBodyElement*)body;
-(DOMElement*)createElement:(NSString*)tag;
-(DOMRange*)createRange;
-(DOMText*)createTextNode:(NSString*)text;
-(DOMElement*)getElementById:(NSString*)ID;
-(DOMNodeList*)getElementsByClassName:(NSString*)name;
-(DOMNodeList*)getElementsByTagName:(NSString*)name;
@end

@interface WebHistoryItem : NSObject
-(WebHistoryItem*)initWithURLString:(NSString*)URLString title:(NSString*)title lastVisitedTimeInterval:(NSTimeInterval)time;
-(NSString*)URLString;
@end

@interface WebHistory : NSObject
-(void)removeItems:(NSArray*)items;
@end

@interface WebBackForwardList : NSObject
-(void)addItem:(WebHistoryItem*)item;
-(NSArray*)backListWithLimit:(int)limit;
-(WebHistoryItem*)currentItem;
-(NSArray*)forwardListWithLimit:(int)limit;
-(void)removeItem:(WebHistoryItem*)item;
@end

@interface WebResource : NSObject
-(WebResource*)initWithData:(NSData*)data URL:(NSURL*)URL MIMEType:(NSString*)MIMEType textEncodingName:(NSString*)encodingName frameName:(NSString*)frameName;
-(NSData*)data;
-(NSString*)frameName;
-(NSString*)MIMEType;
-(NSString*)textEncodingName;
-(NSURL*)URL;
@end

@interface WebArchive : NSObject
-(WebArchive*)initWithMainResource:(WebResource*)mainResource subresources:(NSArray*)subresources subframeArchives:(NSArray*)subframeArchives;
-(WebArchive*)initWithData:(NSData*)data;
-(NSData*)data;
-(WebResource*)mainResource;
-(NSArray*)subframeArchives;
-(NSArray*)subresources;
@end

@class WebFrame;
@interface WebDataSource : NSObject
-(NSData*)data;
-(NSURLRequest*)initialRequest;
-(NSString*)pageTitle;
-(void)addSubresource:(WebResource*)subresource;
-(NSMutableURLRequest*)request;
-(NSURLResponse*)response;
-(WebResource*)subresourceForURL:(NSURL*)URL;
-(WebArchive*)webArchive;
-(WebFrame*)webFrame;
@end

@interface WebFrame : NSObject
-(WebDataSource*)dataSource;
-(DOMDocument*)DOMDocument;
-(void)loadArchive:(WebArchive*)archive;
-(void)loadData:(NSData*)data MIMEType:(NSString*)MIMEType textEncodingName:(NSString*)encodingName baseURL:(NSURL*)URL;
-(void)loadHTMLString:(NSString*)HTMLstring baseURL:(NSURL*)URL;
@end

@interface WebPreferences : NSObject
@end

@interface WebView
@property(assign) WebPreferences* preferences;
@property(assign) id downloadDelegate,frameLoadDelegate,policyDelegate,resourceLoadDelegate,UIDelegate;
+(BOOL)canShowMIMEType:(NSString*)MIMEType;
-(WebBackForwardList*)backForwardList;
-(id)frameLoadDelegate;
-(WebFrame*)mainFrame;
-(void)replaceSelectionWithMarkupString:(NSString*)markup;
-(void)replaceSelectionWithNode:(DOMNode*)node;
-(void)replaceSelectionWithText:(NSString*)text;
-(NSUInteger)selectionAffinity;
-(DOMRange*)selectedDOMRange;
-(void)setSelectedDOMRange:(DOMRange*)range affinity:(NSUInteger)affinity;
-(void)stopLoading:(id)sender;
-(NSString*)stringByEvaluatingJavaScriptFromString:(NSString*)javascript;
@end
