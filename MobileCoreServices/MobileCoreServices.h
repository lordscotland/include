/*	
	MobileCoreServices.h
	Copyright (c) 2009, Apple Inc. All rights reserved.
*/

#ifndef __MOBILECORESERVICES__

#include <MobileCoreServices/UTCoreTypes.h>
#include <MobileCoreServices/UTType.h>

@interface LSResourceProxy : NSObject
@property(assign) NSString* localizedName;
-(NSData*)iconDataForVariant:(NSInteger)format;
@end

@interface LSApplicationProxy : LSResourceProxy
+(LSApplicationProxy*)applicationProxyForIdentifier:(id)identifier;
-(NSString*)applicationIdentifier;
-(NSURL*)containerURL;
-(NSURL*)resourcesDirectoryURL;
@end

@interface LSDocumentProxy : LSResourceProxy
+(LSDocumentProxy*)documentProxyForName:(NSString*)name type:(NSString*)type MIMEType:(NSString*)MIMEType;
-(NSString*)name;
-(NSString*)MIMEType;
-(NSString*)typeIdentifier;
@end

@interface LSOpenOperation : NSOperation
@end

@interface LSApplicationWorkspace : NSObject
+(LSApplicationWorkspace*)defaultWorkspace;
-(NSArray*)applicationsAvailableForHandlingURLScheme:(NSString*)scheme;
-(NSArray*)applicationsAvailableForOpeningDocument:(LSDocumentProxy*)dproxy;
-(LSOpenOperation*)operationToOpenResource:(NSURL*)URL usingApplication:(NSString*)identifier uniqueDocumentIdentifier:(NSString*)document userInfo:(NSDictionary*)userInfo delegate:(id)delegate;
@end

#endif /* __MOBILECORESERVICES__ */
