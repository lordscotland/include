extern NSString* const PSAccessoryKey;
extern NSString* const PSActionKey;
extern NSString* const PSAlignmentKey;
extern NSString* const PSAppGroupBundleIDKey;
extern NSString* const PSAppGroupDomainKey;
extern NSString* const PSAppSettingsBundleIDKey;
extern NSString* const PSAppSettingsBundleKey;
extern NSString* const PSAutoCapsKey;
extern NSString* const PSAutoCorrectionKey;
extern NSString* const PSBadgeNumberKey;
extern NSString* const PSBestGuesserKey;
extern NSString* const PSBundleCustomIconPathKey;
extern NSString* const PSBundleHasBundleIconKey;
extern NSString* const PSBundleHasIconKey;
extern NSString* const PSBundleIconPathKey;
extern NSString* const PSBundleIsControllerKey;
extern NSString* const PSBundleOverridePrincipalClassKey;
extern NSString* const PSBundlePathKey;
extern NSString* const PSBundleSearchControllerClassKey;
extern NSString* const PSBundleSupportsSearchKey;
extern NSString* const PSBundleTintedIconPathKey;
extern NSString* const PSButtonActionKey;
extern NSString* const PSCancelKey;
extern NSString* const PSCellClassKey;
extern NSString* const PSConfirmationActionKey;
extern NSString* const PSConfirmationCancelActionKey;
extern NSString* const PSConfirmationCancelKey;
extern NSString* const PSConfirmationDestructiveKey;
extern NSString* const PSConfirmationKey;
extern NSString* const PSConfirmationOKKey;
extern NSString* const PSConfirmationPromptKey;
extern NSString* const PSConfirmationTitleKey;
extern NSString* const PSContainerBundleIDKey;
extern NSString* const PSControlIsLoadingKey;
extern NSString* const PSControlKey;
extern NSString* const PSControlMaximumKey;
extern NSString* const PSControlMinimumKey;
extern NSString* const PSControllerLoadActionKey;
extern NSString* const PSCopyableCellKey;
extern NSString* const PSDataSourceClassKey;
extern NSString* const PSDecimalKeyboardKey;
extern NSString* const PSDefaultValueKey;
extern NSString* const PSDefaultsKey;
extern NSString* const PSDeferItemSelectionKey;
extern NSString* const PSDeletionActionKey;
extern NSString* const PSDetailControllerClassKey;
extern NSString* const PSDisplaySortedByTitleKey;
extern NSString* const PSEditPaneClassKey;
extern NSString* const PSEmailAddressKeyboardKey;
extern NSString* const PSEmailAddressingKeyboardKey;
extern NSString* const PSEnabledKey;
extern NSString* const PSFooterAlignmentGroupKey;
extern NSString* const PSFooterCellClassGroupKey;
extern NSString* const PSFooterHyperlinkViewActionKey;
extern NSString* const PSFooterHyperlinkViewLinkRangeKey;
extern NSString* const PSFooterHyperlinkViewTargetKey;
extern NSString* const PSFooterHyperlinkViewTitleKey;
extern NSString* const PSFooterHyperlinkViewURLKey;
extern NSString* const PSFooterTextGroupKey;
extern NSString* const PSFooterViewKey;
extern NSString* const PSGetterKey;
extern NSString* const PSHeaderCellClassGroupKey;
extern NSString* const PSHeaderDetailTextGroupKey;
extern NSString* const PSHeaderViewKey;
extern NSString* const PSHidesDisclosureIndicatorKey;
extern NSString* const PSIDKey;
extern NSString* const PSIPKeyboardKey;
extern NSString* const PSIconImageKey;
extern NSString* const PSIsPerGizmoKey;
extern NSString* const PSIsRadioGroupKey;
extern NSString* const PSKeyNameKey;
extern NSString* const PSKeyboardTypeKey;
extern NSString* const PSLazilyLoadedBundleKey;
extern NSString* const PSLocalizableMesaStringForKey;
extern NSString* const PSLocalizableStockholmStringForKey;
extern NSString* const PSManifestEntriesKey;
extern NSString* const PSManifestSectionKey;
extern NSString* const PSManifestStringTableKey;
extern NSString* const PSMarginWidthKey;
extern NSString* const PSNegateValueKey;
extern NSString* const PSNotifyNanoKey;
extern NSString* const PSNumberKeyboardKey;
extern NSString* const PSPaneTitleKey;
extern NSString* const PSPlaceholderKey;
extern NSString* const PSPlistNameKey;
extern NSString* const PSPrioritizeValueTextDisplayKey;
extern NSString* const PSRadioGroupCheckedSpecifierKey;
extern NSString* const PSRequiredCapabilitiesKey;
extern NSString* const PSRequiredCapabilitiesOrKey;
extern NSString* const PSSetterKey;
extern NSString* const PSSetupCustomClassKey;
extern NSString* const PSSetupFinishedAllStepsKey;
extern NSString* const PSShortTitlesDataSourceKey;
extern NSString* const PSShortTitlesKey;
extern NSString* const PSSliderLeftImageKey;
extern NSString* const PSSliderLeftImagePromiseKey;
extern NSString* const PSSliderRightImageKey;
extern NSString* const PSSliderRightImagePromiseKey;
extern NSString* const PSSliderShowValueKey;
extern NSString* const PSSpecifierActionKey;
extern NSString* const PSSpecifierAuthenticationTokenKey;
extern NSString* const PSSpecifierIsSearchableKey;
extern NSString* const PSSpecifierIsSectionKey;
extern NSString* const PSSpecifierPasscodeKey;
extern NSString* const PSSpecifierSearchBundleKey;
extern NSString* const PSSpecifierSearchKeywordsKey;
extern NSString* const PSSpecifierSearchPlistKey;
extern NSString* const PSSpecifierSearchTitleKey;
extern NSString* const PSSpecifierSupportsSearchToggleKey;
extern NSString* const PSSpecifiersKey;
extern NSString* const PSStaticTextMessageKey;
extern NSString* const PSStringsTableKey;
extern NSString* const PSTableCellClassKey;
extern NSString* const PSTableCellHeightKey;
extern NSString* const PSTableCellKey;
extern NSString* const PSTableCellSubtitleTextKey;
extern NSString* const PSTableCellUseEtchedAppearanceKey;
extern NSString* const PSTextFieldNoAutoCorrectKey;
extern NSString* const PSTextViewBottomMarginKey;
extern NSString* const PSTitleKey;
extern NSString* const PSTitlesDataSourceKey;
extern NSString* const PSURLKeyboardKey;
extern NSString* const PSUsageBundleAppKey;
extern NSString* const PSUsageBundleCategoryKey;
extern NSString* const PSUsageBundleDeletingKey;
extern NSString* const PSValidTitlesKey;
extern NSString* const PSValidValuesKey;
extern NSString* const PSValueChangedNotificationKey;
extern NSString* const PSValueKey;
extern NSString* const PSValuesDataSourceKey;
extern NSString* const PSWifiNameKey;
extern NSString* const PSWifiPowerStateKey;
extern NSString* const PSWifiTetheringStateKey;

typedef NS_ENUM(NSInteger,PSCellType) {
  PSDefaultCell=-1,
  PSGroupCell,
  PSLinkCell,
  PSLinkListCell,
  PSListItemCell,
  PSTitleValueCell,
  PSSliderCell,
  PSSwitchCell,
  PSStaticTextCell,
  PSEditTextCell,
  PSSegmentCell,
  PSGiantIconCell,
  PSGiantCell,
  PSSecureEditTextCell,
  PSButtonCell,
  PSEditTextViewCell,
  PSSpinnerCell,
  PSLastCell
};

@interface PSSpecifier : NSObject
@property(assign) NSString* name;
@property(assign) id target;
@property(assign) SEL buttonAction;
+(PSSpecifier*)deleteButtonSpecifierWithName:(NSString*)name target:(id)target action:(SEL)action;
+(PSSpecifier*)emptyGroupSpecifier;
+(PSSpecifier*)groupSpecifierWithName:(NSString*)name;
+(PSSpecifier*)preferenceSpecifierNamed:(NSString*)name target:(id)target set:(SEL)set get:(SEL)get detail:(Class)detail cell:(PSCellType)cell edit:(Class)edit;
-(id)propertyForKey:(NSString*)key;
-(void)setProperty:(id)property forKey:(NSString*)key; 
@end

@interface PSViewController : UIViewController @end
@interface PSListController : PSViewController {
  NSArray* _specifiers;
  UITableView* _table;
}
-(void)reload;
-(void)reloadSpecifier:(PSSpecifier*)specifier;
-(void)reloadSpecifiers;
-(PSSpecifier*)specifier;
-(NSArray*)specifiers;
@end
